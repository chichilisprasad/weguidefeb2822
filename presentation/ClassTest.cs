﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace presentation
{
    class ClassTest
    {
        public static void Main()
        {
            //object declaration
            Employee emp1;
            emp1= new Employee();
            //calling the members of a class
            emp1.name = "Prasad";
            emp1.address = "bangalore";
            emp1.display();
            Employee emp2 = new Employee();
            emp2.name = "kailash";
            emp2.address = "Channai";
            emp2.display();
            Console.ReadKey();
        }
    }
    class Employee
    {
        //fields- store the values
        internal string name;
        internal string address;
        //function - to do some thing
        internal void display()
        {
            Console.WriteLine("Employee name is {0} and employee resides in {1} address",name,address);
        }
        private int add(int a,int b)
        {
            return a + b;
        }
    }
}
