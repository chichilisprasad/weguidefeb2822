﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace presentation
{
    class Employee : Person
    {
        public double Sal { get; set; }

        internal override void Display(string name, string address)
        {
            Name = name;
            Address = address;
            Console.WriteLine("Employee {0} is residing in {1} address and earning {2} salary", Name, Address,Sal);

        }

        internal new void Display()
        {
            Console.WriteLine("Employee {0} is residing in {1} address and earning {2} salary", Name, Address, Sal);
        }
    }
}
