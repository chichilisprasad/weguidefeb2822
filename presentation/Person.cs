﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace presentation
{
    class Person
    {
        //fields
        string name;
        //string address;
        internal string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value + " Reddy";
            }
        }
        //internal string Address
        //{
        //    get
        //    {
        //        return address;
        //    }
        //    set
        //    {
        //        address = value ;
        //    }
        //}
        public string Address { get; set; }
        //Methods
        internal void Display()
        {
            Console.WriteLine("Person {0} is residing in {1} address",Name,Address);
        }
        internal void Display(string Name)
        {
            this.Name = Name;
            Console.WriteLine("Person {0} is residing in {1} address", Name, Address);
            
        }
        internal virtual void Display(string name,string address)
        {
            Name = name;
            Address = address;
            Console.WriteLine("Person {0} is residing in {1} address", Name, Address);

        }
    }
}
