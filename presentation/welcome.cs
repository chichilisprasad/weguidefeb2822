﻿using System;
using System.Collections;
using System.Text;

namespace presentation
{
    class welcome
    {
    }
    class A
    {
        public static void Main()
        {
            /*Console.WriteLine("welcome to .Net world");
            Console.ReadLine();*/

            #region read data from console
            //Console.WriteLine("Enter your name");
            //string name = Console.ReadLine();
            //Console.WriteLine("welcome "+name);
            ////Console.ReadLine();
            //Console.ReadKey();
            #endregion

            #region reading interger values
            //Console.WriteLine("Enter first value");
            //int a =Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("Enter second value");
            //int b = Convert.ToInt32(Console.ReadLine());
            ////declaration
            //int c;
            ////intialization
            //c = a + b;
            //Console.WriteLine("Sum:" + c);
            //Console.ReadKey();
            #endregion

            #region operators
            //int i = 10;
            //Console.WriteLine(++i); //11
            //Console.WriteLine(i); //11
            //Console.WriteLine(i++);//11
            //Console.WriteLine(++i);//13
            //Console.WriteLine(i);//13
            //Console.ReadKey();

            //int i = 10 % 3;
            //Console.WriteLine(i);
            //Console.ReadKey();
            //int i = (3 + 4) - 5 / 4 *2;
            //Console.WriteLine(i);
            //Console.ReadKey();
            //BODMAS 
            //B- Bracket
            //O -of
            // D- divsion
            // M - Multiplication
            //A- addition
            //S- substraction

            #endregion
            #region control statements
            //Console.WriteLine("Enter value");
            //int a = Convert.ToInt32(Console.ReadLine());
            //if(a < 10)
            //{
            //    Console.WriteLine(a+" value is less than 10");
            //}
            //else if(a > 10 & a < 40)
            //{
            //    Console.WriteLine(a+" is greter than 10 and less than 40");
            //}
            //else
            //{
            //    Console.WriteLine(a+" value is greater than 40");
            //}
            //Console.ReadKey();
            #endregion

            #region iterative statement- while
            ////1.intialization
            //int i = 0;
            ////2.validate the expression
            //while(i<10)
            //{
            //    Console.WriteLine(" welcome to weguide technologies");
            //    //increment or decrement
            //    i = i + 2;
            //}

            //Console.ReadKey();
            #endregion

            #region dowhile
            //1.initialization
            //int i = 10;
            //do
            //{
            //    Console.WriteLine(i+"welcome to weguide technologies");
            //    i++;
            //} while (i < 10);
            //Console.ReadKey();
            #endregion

            #region for loop
            //for(int i=0;i<10;i++)
            //{
            //    Console.WriteLine(i+" welcome to weguide technologies");
            //}
            //Console.ReadKey();
            #endregion
            #region Boxing and unboxing
            //int i = 10;
            //string str = string.Empty;
            ////boxing
            //str = i.ToString();
            //str = "10";
            ////unboxing
            //i = Convert.ToInt32(str);
            #endregion

            #region var vs synamic
            //var i = 10;
            //Console.WriteLine(i.GetType());
            //var j = 20.7;
            //Console.WriteLine(j.GetType());
            ////var name;
            ////name = "Prasad";
            //dynamic name;
            //name = "Prasad";
            //Console.WriteLine(name.GetType());
            //Console.ReadLine();
            #endregion

            #region string substitution
            //Console.WriteLine("Enter first name");
            //string fname = Console.ReadLine();
            //Console.WriteLine("Enter last name");
            //string lname = Console.ReadLine();
            //Console.WriteLine("Enter location");
            //string location = Console.ReadLine();
            ////Console.WriteLine("Employee "+fname +" "+lname+" resides in "+location+" location");
            //Console.WriteLine("Employee {0} {1} resides in {2} location",fname,lname,location);
            //Console.ReadKey();

            #endregion
            #region String function
            //string str = "welcome to weguide technologies";
            ////string str = Console.ReadLine();
            //Console.WriteLine("length: " + str.Length);
            //Console.WriteLine("substring: "+str.Substring(11));
            //Console.WriteLine("substring: " + str.Substring(11,5));
            //Console.WriteLine(str.ToLower());
            //Console.WriteLine(str.ToUpper());
            //Console.WriteLine(str);
            //Console.WriteLine(str.TrimStart('w'));
            //Console.ReadKey();
            #endregion

            #region string vs String builder
            //Console.WriteLine("--string ---immutable");
            //string str = "Prasad";
            //str = str + " Reddy";
            //Console.WriteLine("your name:{0}",str);

            //Console.WriteLine("--string builder---Mutable");
            //StringBuilder sb = new StringBuilder("Prasad");
            //sb.Append(" Reddy");
            //Console.WriteLine("your name:{0}",sb.ToString());
            //Console.ReadKey();

            #endregion
            #region Array
            /* 
             1. Array is a collection of similar data type items
             2. You need to specify size of an array
             3. You assign values to array by using a index of a array
             4. always array starts with 0th index
             */
            //int[] marks = new int[10];
            //marks[0] = 89;
            //marks[1] = 90;
            //marks[2] = 67;
            //marks[4] = 65;
            //marks[5] = 89;
            //for(int i=0;i<marks.Length;i++)
            //{ 
            //Console.WriteLine(i+" "+marks[i]);
            //}
            //Console.ReadKey();

            //string[] cities = new string[3];
            //cities[0] = "Bangalore";
            //cities[1] = "Chennai";
            //cities[2] = "Hyderabad";
            //Console.WriteLine("--for each---");
            //foreach (string city in cities)
            //{
            //    Console.WriteLine(city);
            //}
            //Console.WriteLine("---for-----");
            //for(int i =1;i<cities.Length;i++)
            //{
            //    Console.WriteLine(cities[i]);
            //}
            //Console.ReadKey();
            #endregion
            #region Arraylist
            //1. Array list is dynamic
            //2. it is not collection of similar datatype items
            //ArrayList countries = new ArrayList();
            //countries.Add("India");
            //countries.Add("USA");
            //countries.Add("China");
            ////countries.Add(23);
            ////countries.Add(34.45);
            ////Console.WriteLine(countries[0]);
            //foreach(string country in countries)
            //{
            //    Console.WriteLine(country);
            //}
            //Console.ReadKey();
            #endregion
            #region ArrayListinteger
            //ArrayList marks = new ArrayList
            //{
            //    23,
            //    31,
            //    56,
            //    78
            //};
            //marks.Add(67);
            //foreach(var mark in marks)
            //{
            //    Console.WriteLine(Convert.ToInt32(mark));
            //}
            //Console.ReadKey();
            #endregion
            #region 
            ArrayList countries = new ArrayList
            {
                "India",
                "USA",
                "China"
            };
            foreach(var country in countries)
            {
                Console.WriteLine(country);
            }
            if(countries.Contains("India1"))
            {
                Console.WriteLine("India is part of coutries");
            }
            else
            {
                Console.WriteLine("India1 is not part of countries");
            }
            countries.Remove("China");
            Console.WriteLine("----------------");
            foreach (var country in countries)
            {
                Console.WriteLine(country);
            }
            Console.ReadKey();
            #endregion

        }
    }
}
